Getting started
=================
To use the Sangaboard motor controller from Python, you will first need to install this module.  It can be installed using `pip` in the usual way, which will also require the packages that it depends on (:mod:`pyserial`).  The simplest way to use the module is this::

   from sangaboard import Sangaboard
   with Sangaboard() as sb:
       sb.move_rel([1000,0,0])
       print(sb.position)
       sb.move_rel([-1000,0,0])
   
By default, it will use the first available serial port - if you are using a Raspberry Pi and you don't have any other USB serial devices connected, this will usually work.  If not, you need to specify the serial port in the constructor::

   Sangaboard("/dev/ttyUSB0")
   
The name of the serial port will depend on your operating system - Linux typically assigns names that look like ``/dev/ttyUSB0`` while Windows will often give it a name like ``COM4``.

Make sure you close the Sangaboard after you're finished with it - the best way to do this is using a `with` block, but you can also call :meth:`~.BasicSerialInstrument.close` manually if required.

Running motors
-----------------
The most basic thing you are likely to want to do with the Sangaboard.  This is done with :meth:`~.Sangaboard.move_rel` most of the time, though it's also possible to make absolute moves.  The Sangaboard keeps track of position in firmware, and will return its position if you query :attr:`~.Sangaboard.position`.

Adjusting settings
--------------------
There are a number of properties of your :class:`~.Sangaboard` object that can be used to change the way it works:

* :attr:`~.Sangaboard.ramp_time`: acceleration control

* :attr:`~.Sangaboard.step_time`: define the maximum speed

Using optional features
-------------------------
The Sangaboard is designed to also allow communication to I2C devices. These require special firmware to be compiled in the Ardunio IDE. Support for optional features which are in the firmware, but are not compiled as standard are included in this library.

If you compile support for it, you can add a light sensor to the Sangaboard, which is accessed as :attr:`~.Sangaboard.light_sensor`.  This returns a :class:`.LightSensor` which allows you to control the gain (if possible) and read the light intensity.

If you compile support for it, you can add endstops to the Sangaboard, which are accessed as :attr:`~.Sangaboard.endstops`. This returns :class:`.Endstops` which allows you to home the axes, check endstop status, and control soft endstop position (if enabled).
