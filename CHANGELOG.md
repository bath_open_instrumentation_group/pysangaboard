# Changelog for pysangaboard

This changelog started after v0.2.3

## v0.3.3
* Extended autodetection to try both 115200 and 38400 baud on each port, to enable more reliable comms with the Pi HAT.

## v0.3.2
* Added a work-around to autodetect boards plugged into the built-in serial port on a Raspberry Pi's GPIO header.

## v0.3.1
* Fixed a typo that caused incorrect behaviour with blocking moves on firmware v1.0.

## v0.3.0
* Added support for Sangaboard firmware v1.0.  ([!10](https://gitlab.com/bath_open_instrumentation_group/pysangaboard/-/merge_requests/10))
  * Currently, we enable blocking moves when we connect to the board, and then use it in the same way as previous boards.  After the initial command to enable blocking moves, it behaves identically to older firmwares (for valid input).
  * Swapped ad-hoc regular expressions for `semantic_version` when parsing firmware versions.
  * Added unit tests 

